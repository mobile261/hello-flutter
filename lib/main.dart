import 'package:flutter/material.dart';
void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();
}
String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String koreanGreeting = "헬로 플러터";
String chineseGreeting = "你好颤振";

class _MyStatefulWidgetState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;

    @override
  Widget build(BuildContext context) {
  return MaterialApp(
    debugShowCheckedModeBanner: false,
    //Scaffold Widget
    home: Scaffold(
      appBar: AppBar(
          title: Text("Hello Flutter "),
          leading: Icon(Icons.home),
          actions: <Widget>[
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting?
                    spanishGreeting : englishGreeting;
                  });
                },
                icon: Icon(Icons.refresh)),

            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting?
                    koreanGreeting : englishGreeting;
                  });
                },
                icon: Icon(Icons.radar)),

            IconButton(
                onPressed: () {
                  setState(() {
                    displayText = displayText == englishGreeting?
                    chineseGreeting : englishGreeting;
                  });
                },
                icon: Icon(Icons.abc))
          ],
      ),
      body:Center(
          child: Text(displayText,
          style:TextStyle(fontSize: 24)),
          ),
    ),
  );
}
}










// class HelloFlutterApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//   return MaterialApp(
//     debugShowCheckedModeBanner: false,
//     //Scaffold Widget
//     home: Scaffold(
//       appBar: AppBar(
//           title: Text("Hello Flutter "),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(onPressed: () {},
//             icon: Icon(Icons.refresh))
//           ],
//       ),
//       body:Center(
//           child: Text("Hello Flutter",
//           style:TextStyle(fontSize: 24)),
//     ),
//     ),
//   );
// }
// }